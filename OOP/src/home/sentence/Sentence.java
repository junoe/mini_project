package home.sentence;
import java.util.ArrayList;


public class Sentence {
	
	private String sentence;
	private ArrayList<String> tag = new ArrayList<String>();
	
	public Sentence() {
		// TODO Auto-generated constructor stub
	}
	
    public ArrayList<String> getTag() {
		return tag;
	}
    
	public void setTag(ArrayList<String> tag) {
		this.tag = tag;
	}
	
    public String getSentence() {
		return sentence;
	}
    
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	
	// check da co tag nay r k can add
	public void addTag(String ...tagName) {
		for(String word: tagName) {
			tag.add(word);
		}
		
	}
}
