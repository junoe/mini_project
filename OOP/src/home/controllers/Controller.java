package home.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import home.handler.Model;
import home.search.Search;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
public class Controller {
	
	Model model = Model.getInstance();
	public Controller() {

    }

	
	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private Button home;
	
	@FXML
	private ImageView homeImage;

	// scene2
	
	

	Search search = new Search();

	@FXML
    private TextField text;

	@FXML
	private Button find;

	@FXML
	private HBox hbox;

	@FXML
	private TextArea textOutput;
         
	@FXML
	private Button clear;
        
	@FXML
	private FlowPane keyPane;
	
		
	
	@FXML
	public void enter(KeyEvent e) {
		if (e.getCode().toString().equals("ENTER")) {
			
			search.addTag(text.getText().toLowerCase());
			Button lb = new Button(text.getText().toLowerCase());
			lb.setStyle("-fx-background-color:ef8891;" + "-fx-text-fill:#ffffff;");
			hbox.getChildren().add(lb);
			System.out.println(text.getText());
			text.clear();
		}

	}
    
	// out put tag
	@FXML
	public void display(ActionEvent event) throws FileNotFoundException, UnsupportedEncodingException, IOException {

		ArrayList<String> result = search.getTag();
        
		ArrayList<String> answer = model.handle(result);
		hbox.setSpacing(20.0);
		for (String res : answer) {
				textOutput.appendText(res + "\n");
		}
	
	}

	// load scene 2
	@FXML
	public void click() {
		try {
			
			Parent root = (Parent) FXMLLoader.load(getClass().getResource("scene2.fxml"));
			Stage stage = (Stage) home.getScene().getWindow();
			stage.setScene(new Scene(root));
			
					
	        stage.setMaxHeight(900);
	        stage.setMaxWidth(1300);
		    stage.show();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
     
	@FXML
	public void clear() {
		hbox.getChildren().removeAll(hbox.getChildren());
		//text.clear();
		textOutput.clear();
		search.removeAllTag();
	}
	
	
    @FXML 
    public void collectTag(MouseEvent e) {
    	search.addTag(((Button) e.getSource()).getText());
    	Button lb = new Button(((Button) e.getSource()).getText().toLowerCase());
		lb.setStyle("-fx-background-color:ef8891;" + "-fx-text-fill:#ffffff;");
		hbox.getChildren().add(lb);
    }
	
    //insert part
     @FXML
     private TextField bar;
     
     @FXML 
     private Button ok;


}
