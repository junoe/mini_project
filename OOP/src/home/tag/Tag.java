package home.tag;

import java.util.ArrayList;

public abstract class Tag {

	protected String name;
	protected ArrayList<String> wordBag = new ArrayList<String>();
	
	public Tag() {
	
	}
	
	public Tag(String name) {
		this.name = name;
		// TODO Auto-generated constructor stub
	}
	

    public String getName() {
		return name;
	}
    
    /*
	public void setName(String name) {
		this.name = name;
	}
	*/
	public ArrayList<String> getWordBag() {
		return wordBag;
	}
	
	public void addWord(String key) {
		wordBag.add(key);
    }
	/*
	public void setWordBag(ArrayList<String> wordBag) {
		this.wordBag = wordBag;
	}
	*/
	

	/*
	
	*/
	
	
	
}
