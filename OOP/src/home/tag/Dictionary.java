package home.tag;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import home.handler.Reader;

public class Dictionary {
	private static Dictionary single_instance = null;
	private ArrayList<Tag> tagList = new ArrayList<Tag>();
	/*
	public Dictionary() {
		generateDict();
		// TODO Auto-generated constructor stub
	}*/
	
	private Dictionary() {
		try {
			generateDict();
	    }catch(Exception e) {
			e.printStackTrace();
		}
		
		// TODO Auto-generated constructor stub
	  }
	 
	 
	public static Dictionary getInstance()
    {
        if (single_instance == null)
            single_instance = new Dictionary();
  
        return single_instance;
    }
	
 
    
    
	public ArrayList<Tag> getTagList() {
		return tagList;
	}
	public void setTagList(ArrayList<Tag> tagList) {
		this.tagList = tagList;
	}
	
	
	public void addTag(Tag newTag) {
		
		tagList.add(newTag);
		
	}
	
	
    public void removeTag(Tag tag) {
		
		tagList.remove(tag);
		
	}
	public void generateDict() throws FileNotFoundException, UnsupportedEncodingException, IOException{
		// add category
		//ArrayList<String> categoryInput = Reader.readUTF8Text("C:\\Users\\Admin\\eclipse-workspace\\OOP\\src\\home\\file\\category.txt");
		Reader read = new Reader();
		ArrayList<String> categoryInput = read.readUTF8Text("/home/file/category.txt");
		for(String in:categoryInput ){
			String[] tagInput = in.split(",");
			Category cate = new Category();
		
			for(int i=0;i<tagInput.length;i++) {
				if(i==0) {
			       cate.setName(tagInput[i]);
				}else cate.addWord(tagInput[i]);
			}
			tagList.add(cate);
		}
		
		// add stock
		// ArrayList<String> stockInput = Reader.readUTF8Text("C:\\Users\\Admin\\eclipse-workspace\\OOP\\src\\home\\file\\stock.txt");
		 ArrayList<String> stockInput = read.readUTF8Text("/home/file/stock.txt");
		for(String in:stockInput) {
			String[] tagInput = in.split(",");
			Stock stock = new Stock();
		
			for(int i=0;i<tagInput.length;i++) {
				if(i==0) {
			       stock.setName(tagInput[i]);
				}else stock.addWord(tagInput[i]);
			}
			tagList.add(stock);
		}
	
	}
	
	public void display() {
		for(Tag tag:tagList) {
			System.out.println(tag.getName());
			for(String word:tag.getWordBag()) {
				System.out.println(word);
			}
		}
	}
	
}
