package home.handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

public class Reader {

    public Reader() {
    }

    public  ArrayList<String> readUTF8Text(String file)
            throws FileNotFoundException, UnsupportedEncodingException, IOException {
        //FileInputStream fi = new FileInputStream(file);
        
        InputStream fi = getClass().getResourceAsStream(file);
        InputStreamReader isr = new InputStreamReader(fi, "utf-8");
        
        BufferedReader br = new BufferedReader(isr);
        ArrayList<String> obList = new ArrayList<>();

        String text = null;
        while ((text = br.readLine()) != null) {
            obList.add(text);
        }
        fi.close();
        
        
        return obList;
    }

}