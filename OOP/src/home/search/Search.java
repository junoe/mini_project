package home.search;

import java.util.ArrayList;

public class Search {
	public int index =0;
	private ArrayList<String> tagSearch = new ArrayList<String>();
    
	public Search() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getTag() {
		return tagSearch;
	}

	public void addTag(String s) {
		int found = 0;
		
		for (int i = 0; i < tagSearch.size(); i++) {
			if (tagSearch.get(i).equals(s)) {
				System.out.println("Name exist");
				found = 1;
				break;
			}
		}
		if (found == 0) {
			tagSearch.add(s);
			System.out.println("The tag has been added.");
			index++;
		}
	}

	// each time search remove all for next search
	public void removeAllTag() {
		for (int i = 0; i < tagSearch.size(); i++) {
			tagSearch.remove(i);
		}
	}
	
	//undo the previous tag
	public void removePreviousTag() {
		tagSearch.remove(index);
		index--;
		System.out.println("A tag was undone.");
	}

	public void dis() {
		for (String s : tagSearch) {
			System.out.println(s);
		}
	}

}
