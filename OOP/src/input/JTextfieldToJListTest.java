package input;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;

import javax.swing.*;
public class JTextfieldToJListTest extends JFrame {
	static String a;
   private DefaultListModel model;
   private JList list;
   private JTextField jtf;
   public JTextfieldToJListTest() {
	  
      setTitle("JTextfieldToJList Test");
      model = new DefaultListModel();
      jtf = new JTextField("Type something and Hit Enter");
      jtf.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent me) {
            jtf.setText("");
         }
      });
      list = new JList(model);
      list.setBackground(Color.lightGray);
      jtf.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent event) {
            model.addElement(jtf.getText());
            JOptionPane.showMessageDialog(null, jtf.getText());
            a = jtf.getText();
            FileWriter fw = new FileWriter(new File("src\\home\\file\\added.txt"));
            fw.write(a);
            fw.close();
            jtf.setText("Type something and Hit Enter");
         }
      });
      add(jtf,BorderLayout.NORTH);
      add(new JScrollPane(list),BorderLayout.CENTER);
      setSize(375, 250);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLocationRelativeTo(null);
      setVisible(true);
   }
   public static void main(String[] args) {
      new JTextfieldToJListTest();
      System.out.print(a);
   }
}