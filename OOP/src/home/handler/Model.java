package home.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import home.sentence.Sentence;
import home.tag.*;

public class Model  {
	ArrayList<Sentence> search = new ArrayList<Sentence>();
	private static Model single = null;

	
	private Model() {
		try {
	         init();
	    }catch(Exception e) {
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}
	public static Model getInstance()
    {
        if (single == null)
            single = new Model();
  
        return single;
    }
    
	public void init() throws FileNotFoundException, UnsupportedEncodingException, IOException {
		//ArrayList<String> input = Reader.readUTF8Text("C:\\Users\\Admin\\eclipse-workspace\\OOP\\src\\home\\file\\over.txt");
		Reader read = new Reader();
		ArrayList<String> input = read.readUTF8Text("/home/file/over.txt");
        
		Dictionary temp = Dictionary.getInstance();

		ArrayList<Tag> tagDict = temp.getTagList();

	

		for (int j = 0; j < input.size(); j++) {
			Sentence sentence = new Sentence();
			sentence.setSentence(input.get(j));

			for (int i = 0; i < tagDict.size(); i++) {

				ArrayList<String> tagList = tagDict.get(i).getWordBag();
				for (String tag : tagList) {
					if ((input.get(j)).toLowerCase().contains(tag)) {
						sentence.addTag(tagDict.get(i).getName());
					}
				}
			}
			search.add(sentence);
		}
	}	
		
		/*
		 * for(Sentence sent:searchResult) { System.out.println(sent.getSentence()); }
		 */
	public void display() {
		for(Sentence sen:search) {
			System.out.println(sen.getSentence());
			for(String s:sen.getTag()) {
				System.out.print(s);
			}
		}
	}
	
	
	
    public  ArrayList<String> handle(ArrayList<String> tagSearch){
		ArrayList<String> result = new ArrayList<String>();
		// moi cai tag o search trung voi tag o dict thi return  // neu dung hon  1 nua tag thi add mau cau vao
		int count = 0;
		for (Sentence sent : search) {
		    	
			     for (String tag:tagSearch) {
		              if(sent.getTag().contains(tag.toLowerCase())) {
		            	  count++;
		              }
		              if(count == tagSearch.size()) {
						   result.add(sent.getSentence());
						   count = 0;
					  }
				 }
			     count = 0;
			 
	 } 
		
		
     return result;
	}
}
